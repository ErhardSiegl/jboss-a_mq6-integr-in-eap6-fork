#!/bin/sh 
SHOWCASE="JBoss Red Hat EAP 6 integration with Red Hat JBoss A-MQ 6"
cd `dirname $0`
BASE_DIR=`pwd`
JBOSS_HOME=$BASE_DIR/target/jboss-eap-6.1
JBOSS_AMQ_HOME=$BASE_DIR/target/jboss-a-mq-6.0.0.redhat-024
RAR_HOME=$BASE_DIR/target/rar
ACTIVEMQ_HOME=$BASE_DIR/target/activemq
SERVER_DIR=$JBOSS_HOME/standalone/deployments/
SERVER_CONF=$JBOSS_HOME/standalone/configuration/
SRC_DIR=${DOWNLOADS-"$BASE_DIR/installs"}
PRJ_DIR=$BASE_DIR/projects/eap6-integr-jboss-a_mq
EAP=jboss-eap-6.1.0.Beta.zip
AMQ=jboss-a-mq-6.0.0.redhat-024.zip
EAP_VERSION=6.1.0.BETA
AMQ_VERSION=6.0.0.GA
INT_AMQ_VERSION=5.8.0.redhat-60024

echo
echo "Setting up the ${SHOWCASE} in ${BASE_DIR} ..."
echo

cd $BASE_DIR

# make some checks first before proceeding.	
# JBoss EAP
if [[ -r $SRC_DIR/$EAP || -L $SRC_DIR/$EAP ]]; then
		echo Red Hat JBoss EAP binary is present...
		echo
else
		echo Need to download $EAP package from the Customer Support Portal 
		echo and place it in the $SRC_DIR directory to proceed...
		echo
		exit
fi
# JBoss A-MQ
if [[ -r $SRC_DIR/$AMQ || -L $SRC_DIR/$AMQ ]]; then
		echo Red Hat JBoss A-MQ binary is present...
		echo
else
		echo Need to download $AMQ package from the Customer Support Portal 
		echo and place it in the $SRC_DIR directory to proceed...
		echo
		exit
fi

# Create the target directory if it does not already exist.
if [ ! -x target ]; then
		echo "  - creating the target directory..."
		echo
		mkdir target
else
		echo "  - detected target directory, moving on..."
		echo
fi

if [ ! -x $RAR_HOME ]; then
		echo "  - creating the rar directory..."
		echo
		mkdir target/rar
else
		echo "  - detected rar directory, moving on..."
		echo
fi

if [ ! -x $ACTIVEMQ_HOME ]; then
		echo "  - creating the activemq directory..."
		echo
		mkdir target/activemq
else
		echo "  - detected activemq directory, moving on..."
		echo
fi

# Move the old JBoss EAP instance, if it exists, to the OLD position.
if [ -x $JBOSS_HOME ]; then
		echo "  - existing Red Hat JBoss EAP 6 detected..."
		echo
		echo "  - removing existing Red Hat JBoss EAP 6 and install another one..."
		echo
		rm -rf $JBOSS_HOME
		
		# Unzip the JBoss EAP instance.
		echo Unpacking Red Hat JBoss EAP 6...
		echo
		unzip -q -d target $SRC_DIR/$EAP
else
		# Unzip the JBoss EAP instance.
		echo Unpacking new Red Hat JBoss EAP 6...
		echo
		unzip -q -d target $SRC_DIR/$EAP
fi

# Move the old JBoss A-MQ instance, if it exists, to the OLD position.
if [ -x $JBOSS_AMQ_HOME ]; then
		echo "  - existing Red Hat JBoss A-MQ 6 detected..."
		echo
		echo "  - removing existing Red Hat JBoss A-MQ 6 and install another one..."
		echo
		rm -rf $JBOSS_AMQ_HOME

		# Unzip the JBoss A-MQ instance.
		echo Unpacking Red Hat JBoss A-MQ 6...
		echo
		unzip -q -d target $SRC_DIR/$AMQ
else
		# Unzip the JBoss A-MQ 6 instance.
		echo Unpacking new Red Hat JBoss A-MQ 6...
		echo
		unzip -q -d target $SRC_DIR/$AMQ
fi

# unzip the apache-activemq-${INT_AMQ_VERSION}-bin.zip in target/activemq
echo Unpacking the standalone apache-activemq-${INT_AMQ_VERSION}-bin.zip...
echo
unzip -q -d $ACTIVEMQ_HOME $JBOSS_AMQ_HOME/extras/apache-activemq-${INT_AMQ_VERSION}-bin.zip

# create a tmp directory in rar and unzip the rar there 
# remove broker-config.xml and copy the MANIFEST.MF support file in tmp directory
# package a customized RAR for the EAP 6.1
echo
echo Packaging a customized ActiveMQ RAR for Red Hat JBoss EAP 6...
echo
mkdir $RAR_HOME/tmp
unzip -q -d $RAR_HOME/tmp $ACTIVEMQ_HOME/apache-activemq-${INT_AMQ_VERSION}/lib/optional/activemq-rar-${INT_AMQ_VERSION}.rar

rm $RAR_HOME/tmp/META-INF/MANIFEST.MF
rm $RAR_HOME/tmp/broker-config.xml

cp support/MANIFEST.MF $RAR_HOME/tmp/META-INF
cp $ACTIVEMQ_HOME/apache-activemq-${INT_AMQ_VERSION}/lib/optional/activemq-jdbc-store-${INT_AMQ_VERSION}.jar $RAR_HOME/tmp/

cd $RAR_HOME/tmp

zip -r ../amq-${INT_AMQ_VERSION}.rar *

cd $BASE_DIR

# copy the standalone-amq580.xml & broker-config.xml support file to JBoss EAP standalone configuration directory
cp support/standalone-amq580.xml $JBOSS_HOME/standalone/configuration
cp support/broker-config.xml $JBOSS_HOME/standalone/configuration
cp target/rar/amq-${INT_AMQ_VERSION}.rar $JBOSS_HOME/standalone/deployments

# copy 3 jars from activemq in to the modules directory

cp $ACTIVEMQ_HOME/apache-activemq-${INT_AMQ_VERSION}/lib/optional/activeio-core-*.jar support/modules/system/layers/base/org/apache/activemq/main
cp $ACTIVEMQ_HOME/apache-activemq-${INT_AMQ_VERSION}/lib/optional/commons-dbcp-*.jar support/modules/system/layers/base/org/apache/activemq/main
cp $ACTIVEMQ_HOME/apache-activemq-${INT_AMQ_VERSION}/lib/optional/commons-pool-*.jar support/modules/system/layers/base/org/apache/activemq/main

# copy the module to JBoss EAP modules
cp -R support/modules $JBOSS_HOME/

echo
echo "Red Hat JBoss A-MQ ${AMQ_VERSION} RAR Integration in Red Hat JBoss EAP ${EAP_VERSION} Complete."
echo
echo You may start the server with:
echo $JBOSS_HOME/bin/standalone.sh -c standalone-amq580.xml
echo or run the tests with:
echo "(cd $BASE_DIR/projects/jboss-a_mq6-integr-in-eap6-I/ && mvn test -P testAll -D jboss.home=$JBOSS_HOME)"
