JBoss A-MQ RAR Integration in JBoss EAP 6 Quickstart Guide
==========================================================

Important: Please note that JBoss A-MQ integration with JBoss EAP 6 is not yet supported by Red Hat and do not use this for your production environment
--------------------------------------------------------------------------------------------------------------------------------------------------------

Integration is based on Red Hat JBoss EAP and Red Hat JBoss A-MQ products.

Setup and Configuration
-----------------------

See Quick Start Guide in project as ODT and PDF for details on installation. For those that can't wait:

- see README in 'installs' directory

- add products 

- run 'setup.sh' & read output

- read Quick Start Guide

- setup JBoss Developer Studio 6.0.1 for project import

- import projects/jboss-a_mq6-intgr-in-eap6-I

- run Arquillian Tests from JBoss Developer Studio 6.0.1 

Released versions
-----------------

See the tagged releases for the following versions of the product:

- v1.0 is JBoss A-MQ 6.0.0, integrating on JBoss EAP 6.1.0.BETA