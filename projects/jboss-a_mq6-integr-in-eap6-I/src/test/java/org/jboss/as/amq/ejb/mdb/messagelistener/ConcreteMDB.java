package org.jboss.as.amq.ejb.mdb.messagelistener;

import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;

/**
 * @author Jaikiran Pai
 */
@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName="destinationType", propertyValue="javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "queue1")
})
public class ConcreteMDB extends CommonBase {

    private static final Logger logger = Logger.getLogger(ConcreteMDB.class);

    public static final String SUCCESS_REPLY = "onMessage() method was invoked";

    public static final String FAILURE_REPLY = "onMessage() method was *not* invoked";

    @Resource(mappedName = "java:jboss/exported/ConnectionFactory")
    private ConnectionFactory factory;

    private Connection connection;
    private Session session;

    @PreDestroy
    protected void preDestroy() throws JMSException {
        session.close();
        connection.close();
    }

    @PostConstruct
    protected void postConstruct() throws JMSException {
        connection = factory.createConnection();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    }

    @Override
    public void onMessage(Message message) {
        logger.info("Received message: " + message);
        try {
            if (message.getJMSReplyTo() != null) {
                logger.info("Replying to " + message.getJMSReplyTo());
                final Destination destination = message.getJMSReplyTo();
                final MessageProducer replyProducer = session.createProducer(destination);
                final Message replyMsg = session.createTextMessage(SUCCESS_REPLY);
                replyMsg.setJMSCorrelationID(message.getJMSMessageID());
                replyProducer.send(replyMsg);
                replyProducer.close();
            }
        } catch (JMSException jmse) {
            throw new RuntimeException(jmse);
        }
    }
}

