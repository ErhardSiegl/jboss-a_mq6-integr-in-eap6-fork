package org.jboss.as.amq.ejb.mdb.cdi;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

/*
import org.jboss.as.test.integration.ejb.mdb.JMSMessagingUtil;
import org.jboss.ejb3.annotation.ResourceAdapter;
*/
import org.jboss.as.amq.ejb.mdb.JMSMessagingUtil;
import org.jboss.logging.Logger;

/**
 * User: jpai
 */
@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = MDBCdiIntegrationTestCase.AMQ_DESTINATION_QUEUE)
})
/*@ResourceAdapter(value = "hornetq-ra.rar")*/
public class CdiIntegrationMDB implements MessageListener {

    private static final Logger logger = Logger.getLogger(CdiIntegrationMDB.class);


    public static final String REPLY = "Successful message delivery!";

    @EJB
    private JMSMessagingUtil jmsMessagingUtil;

    @Inject
    private RequestScopedCDIBean requestScopedCDIBean;

    @Override
    public void onMessage(Message message) {
        logger.info("Received message " + message);
        try {
            if (message.getJMSReplyTo() != null) {
                logger.info("Replying to " + message.getJMSReplyTo());
                // send a reply
                this.jmsMessagingUtil.sendTextMessage(requestScopedCDIBean.sayHello(), message.getJMSReplyTo(), null);
            }
        } catch (JMSException jmse) {
            throw new RuntimeException(jmse);
        }

    }
}