package org.jboss.as.amq.ejb.mdb.cdi;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.TextMessage;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.as.amq.ejb.mdb.JMSMessagingUtil;
import org.jboss.logging.Logger;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Tests that the CDI request scope is active in MDB invocations.
 *
 * @author Stuart Douglas
 * @author Serge Pagop
 */
@RunWith(Arquillian.class)
public class MDBCdiIntegrationTestCase {
	   private static final Logger logger = Logger.getLogger(MDBCdiIntegrationTestCase.class);

	    private static final String REPLY_QUEUE_JNDI_NAME = "java:/queue/MyReplyAMQ";
	    public static final String QUEUE_JNDI_NAME = "java:/queue/MyAMQueue";
	    public static final String AMQ_DESTINATION_QUEUE = "queue1";

	    @EJB(mappedName = "java:module/JMSMessagingUtil")
	    private JMSMessagingUtil jmsUtil;

	    @Resource(mappedName = REPLY_QUEUE_JNDI_NAME)
	    private Queue replyQueue;

	    @Resource(mappedName = QUEUE_JNDI_NAME)
	    private Queue queue;

	    @Deployment
	    public static WebArchive getDeployment() {

	        final WebArchive mdbCdiWar = ShrinkWrap.create(WebArchive.class, "mdb-cdi-integration-test.war");
	        mdbCdiWar.addClasses(CdiIntegrationMDB.class, RequestScopedCDIBean.class, JMSMessagingUtil.class, MDBCdiIntegrationTestCase.class);
	        mdbCdiWar.addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"));
	        logger.info(mdbCdiWar.toString(true));
	        return mdbCdiWar;
	    }


	    @Test
	    public void testRequestScopeActiveDuringMdbInvocation() throws Exception {
	        final String goodMorning = "Good morning";
	        // send as ObjectMessage
	        this.jmsUtil.sendTextMessage(goodMorning, this.queue, this.replyQueue);
	        // wait for an reply
	        final Message reply = this.jmsUtil.receiveMessage(replyQueue, 5000);
	        // test the reply
	        final TextMessage textMessage = (TextMessage) reply;
	        Assert.assertEquals("Unexpected reply message on reply queue: " + this.replyQueue, CdiIntegrationMDB.REPLY, textMessage.getText());
	    }

}
