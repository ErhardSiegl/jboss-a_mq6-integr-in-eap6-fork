package org.jboss.as.amq.ejb.mdb.messagedestination;

import javax.ejb.EJB;
import javax.jms.Message;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.logging.Logger;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Tests message-destination resolution
 * @author Serge Pagop
 *
 */
@RunWith(Arquillian.class)
public class MessageDestinationTestCase {

    private static final Logger logger = Logger.getLogger(MessageDestinationTestCase.class);

    @EJB (mappedName = "java:module/MessagingEjb")
    private MessagingEjb messagingMdb;


    @Deployment
    public static WebArchive getDeployment() {

        final WebArchive mdbTestWar = ShrinkWrap.create(WebArchive.class, "mdb-test.war");
        mdbTestWar.addPackage(MessageDestinationTestCase.class.getPackage());
        mdbTestWar.addAsWebInfResource(MessageDestinationTestCase.class.getPackage(),  "ejb-jar.xml", "ejb-jar.xml");
        mdbTestWar.addAsWebInfResource(MessageDestinationTestCase.class.getPackage(),  "jboss-ejb3.xml", "jboss-ejb3.xml");
        mdbTestWar.addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"));
        logger.info(mdbTestWar.toString(true));
        return mdbTestWar;
    }

    /**
     * Test a deployment descriptor based MDB
     * @throws Exception
     */
    @Test
    public void testMEssageDestinationResolution() throws Exception {
        this.messagingMdb.sendTextMessage("Say hello to " + MessagingEjb.class.getName());
        final Message reply = this.messagingMdb.receiveMessage(5000);
        Assert.assertNotNull("Reply message was null on reply queue", reply);
    }
}