package org.jboss.as.amq.ejb.mdb.distributedtopic;

import org.jboss.as.amq.ejb.mdb.JMSMessagingUtil;
import org.jboss.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

/**
 * User: jpai
 */
@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
		@ActivationConfigProperty(propertyName = "subscriptionDurability", propertyValue = "Durable"),
		@ActivationConfigProperty(propertyName = "clientId", propertyValue = "LoggingBean"),
		@ActivationConfigProperty(propertyName = "subscriptionName", propertyValue = "LoggingBean"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = TopicMDB.TOPIC_PHYSICAL_NAME) })
public class TopicMDB implements MessageListener {

	private static final Logger logger = Logger.getLogger(TopicMDB.class);

	public static final String TOPIC_PHYSICAL_NAME = "topic1";

	public TopicMDB() {
	}

	@EJB
	private JMSMessagingUtil jmsMessagingUtil;

	@Override
	public void onMessage(Message message) {
		logger.info("Received message: " + message);
		if (message instanceof ObjectMessage == false) {
			throw new RuntimeException(this.getClass().getName()
					+ " only accepts ObjectMessage. " + message
					+ " isn't an ObjectMessage");
		}
		try {
			// get the underlying message
			Availability availability = (Availability) ((ObjectMessage) message)
					.getObject();
			/*
			 * if (message.getJMSReplyTo() != null) { logger.info("Replying to "
			 * + message.getJMSReplyTo()); // create a ObjectMessage as a reply
			 * and send it to the reply queue
			 * this.jmsMessagingUtil.sendObjectMessage(underlyingMessage,
			 * message.getJMSReplyTo(), null); }
			 */
			if (availability.isAvailable()) {
				System.out.println(availability.getName() + " is available");
			} else {
				System.out.println(availability.getName() + " is not available");
			}
			System.out.println("---> logging ");

		} catch (JMSException jmse) {
			throw new RuntimeException(jmse);
		}
	}

}
