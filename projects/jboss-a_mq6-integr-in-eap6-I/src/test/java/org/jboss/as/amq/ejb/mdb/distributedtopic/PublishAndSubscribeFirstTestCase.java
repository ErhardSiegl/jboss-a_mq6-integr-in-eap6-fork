package org.jboss.as.amq.ejb.mdb.distributedtopic;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jms.Topic;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.as.amq.ejb.mdb.JMSMessagingUtil;
import org.jboss.logging.Logger;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * Tests message-destination resolution
 * @author Serge Pagop
 */
@RunWith(Arquillian.class)
public class PublishAndSubscribeFirstTestCase {

    private static final Logger logger = Logger.getLogger(PublishAndSubscribeFirstTestCase.class);

    private static final String TOPIC_JNDI_NAME = "java:jboss/eis/ao/ActiveMQTopic";
    
    @EJB (mappedName = "java:module/JMSMessagingUtil")
    private JMSMessagingUtil jMSMessagingUtil;
    
    @Resource(mappedName = TOPIC_JNDI_NAME)
    private Topic topic;

    @Deployment
    public static JavaArchive getDeployment() {

        final JavaArchive ejbJar = ShrinkWrap.create(JavaArchive.class, "publish-and-subscribe1.jar");
        ejbJar.addClasses(Availability.class, JMSMessagingUtil.class, TopicMDB.class, SubscriberBean.class);
        return ejbJar;
    }

    /**
     * Test a deployment descriptor based MDB
     * @throws Exception
     */
    @Test
    public void testMEssageDestinationResolution() throws Exception {
    	Availability availability = new Availability("Tom", true);
        this.jMSMessagingUtil.sendObjectMessage(availability,this.topic,null);
    }
}