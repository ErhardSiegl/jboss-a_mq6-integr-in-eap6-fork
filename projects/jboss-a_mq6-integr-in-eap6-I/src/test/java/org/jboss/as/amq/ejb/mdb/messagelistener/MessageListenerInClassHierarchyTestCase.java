package org.jboss.as.amq.ejb.mdb.messagelistener;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.TextMessage;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.as.amq.ejb.mdb.JMSMessagingUtil;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Tests that if a message listener interface is implemented by the base class of a message driven bean, then
 * it's taken into consideration while checking for message listener interface for the MDB.
 *
 * <p>See https://issues.jboss.org/browse/AS7-2638</p>
 *
 * @author Jaikiran Pai
 */
@RunWith(Arquillian.class)
public class MessageListenerInClassHierarchyTestCase {

    private static final String QUEUE_JNDI_NAME = "java:/queue/MyAMQueue";
    private static final String REPLY_QUEUE_JNDI_NAME = "java:/queue/MyReplyAMQ";

    @EJB(mappedName = "java:module/JMSMessagingUtil")
    private JMSMessagingUtil jmsUtil;

    @Resource(mappedName = QUEUE_JNDI_NAME)
    private Queue queue;

    @Resource(mappedName = REPLY_QUEUE_JNDI_NAME)
    private Queue replyQueue;

    @Deployment
    public static JavaArchive createDeployment() {
        final JavaArchive jar = ShrinkWrap.create(JavaArchive.class, "message-listener-in-class-hierarchy-test.jar");
        jar.addClasses(ConcreteMDB.class, CommonBase.class, JMSMessagingUtil.class);
        return jar;
    }


    /**
     * Test that if a {@link javax.jms.MessageListener} interface is implemented by the base class of a
     * message driven bean, then it's taken into consideration while looking for potential message listener
     * interface
     * <p>See https://issues.jboss.org/browse/AS7-2638</p>
     *
     * @throws Exception
     */
    @Test
    public void testSetMessageDrivenContext() throws Exception {
        this.jmsUtil.sendTextMessage("hello world", this.queue, this.replyQueue);
        final Message reply = this.jmsUtil.receiveMessage(replyQueue, 5000);
        Assert.assertNotNull("Reply message was null on reply queue: " + this.replyQueue, reply);
        final TextMessage textMessage = (TextMessage) reply;
        Assert.assertEquals("setMessageDrivenContext method was *not* invoked on MDB", ConcreteMDB.SUCCESS_REPLY, textMessage.getText());
    }
}
