package org.jboss.as.amq.ejb.mdb.objectmessage;

import org.jboss.as.amq.ejb.mdb.JMSMessagingUtil;
import org.jboss.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

/**
 * User: jpai
 */
@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = MDBAcceptingObjectMessageOfArrayType.QUEUE_JNDI_NAME)
})
public class MDBAcceptingObjectMessageOfArrayType implements MessageListener {

    private static final Logger logger = Logger.getLogger(MDBAcceptingObjectMessageOfArrayType.class);

    public static final String QUEUE_JNDI_NAME = "queue2";

    @EJB
    private JMSMessagingUtil jmsMessagingUtil;

    @Override
    public void onMessage(Message message) {
        logger.info("Received message: " + message);
        if (message instanceof ObjectMessage == false) {
            throw new RuntimeException(this.getClass().getName() + " only accepts ObjectMessage. " + message + " isn't an ObjectMessage");
        }
        try {
            // get the underlying message
            SimpleMessageInEarLibJar[] underlyingMessage = (SimpleMessageInEarLibJar[]) ((ObjectMessage) message).getObject();
            if (message.getJMSReplyTo() != null) {
                logger.info("Replying to " + message.getJMSReplyTo());
                // create a ObjectMessage as a reply and send it to the reply queue
                this.jmsMessagingUtil.sendObjectMessage(underlyingMessage, message.getJMSReplyTo(), null);
            }
        } catch (JMSException jmse) {
            throw new RuntimeException(jmse);
        }
    }

}