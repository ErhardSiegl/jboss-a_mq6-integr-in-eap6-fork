package org.jboss.as.amq.ejb.mdb.messagedrivencontext;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.TextMessage;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.as.amq.ejb.mdb.JMSMessagingUtil;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Tests that the {@link javax.ejb.MessageDrivenBean#setMessageDrivenContext(javax.ejb.MessageDrivenContext)}
 * method is invoked on MDBs which implement the {@link javax.ejb.MessageDrivenBean} interface
 *
 * @author Jaikiran Pai
 * @author Serge Pagop
 */
@RunWith(Arquillian.class)
public class SetMessageDrivenContextInvocationTestCase {

    private static final String QUEUE_JNDI_NAME = "java:/queue/MyAMQueue";
    private static final String REPLY_QUEUE_JNDI_NAME = "java:/queue/MyReplyAMQ";


    @EJB(mappedName = "java:module/JMSMessagingUtil")
    private JMSMessagingUtil jmsUtil;

    @Resource(mappedName = QUEUE_JNDI_NAME)
    private Queue queue;

    @Resource(mappedName = REPLY_QUEUE_JNDI_NAME)
    private Queue replyQueue;

    @Deployment
    public static JavaArchive createDeployment() {

        final JavaArchive jar = ShrinkWrap.create(JavaArchive.class, "set-message-driven-context-invocation-test.jar");
        jar.addClasses(SimpleMDB.class, JMSMessagingUtil.class);
        return jar;
    }

    /**
     * Test that the {@link javax.ejb.MessageDrivenBean#setMessageDrivenContext(javax.ejb.MessageDrivenContext)}
     * was invoked
     *
     * @throws Exception
     */
    @Test
    public void testSetMessageDrivenContext() throws Exception {
        this.jmsUtil.sendTextMessage("hello world", this.queue, this.replyQueue);
        final Message reply = this.jmsUtil.receiveMessage(replyQueue, 5000);
        Assert.assertNotNull("Reply message was null on reply queue: " + this.replyQueue, reply);
        final TextMessage textMessage = (TextMessage) reply;
        Assert.assertEquals("setMessageDrivenContext method was *not* invoked on MDB", SimpleMDB.SUCCESS_REPLY, textMessage.getText());
    }
}
