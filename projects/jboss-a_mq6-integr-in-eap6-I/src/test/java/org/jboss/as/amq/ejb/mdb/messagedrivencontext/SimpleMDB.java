package org.jboss.as.amq.ejb.mdb.messagedrivencontext;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJBException;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.jboss.logging.Logger;

/**
 * @author Jaikiran Pai
 */
@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "queue1")
        
})
public class SimpleMDB implements MessageDrivenBean, MessageListener {

	private static final long serialVersionUID = -1467123653801610867L;

	private static final Logger logger = Logger.getLogger(SimpleMDB.class);

    public static final String SUCCESS_REPLY = "setMessageDrivenContext() method was invoked";

    public static final String FAILURE_REPLY = "setMessageDrivenContext() method was *not* invoked";

    @Resource(mappedName = "java:jboss/exported/ConnectionFactory")
    private ConnectionFactory factory;

    private Connection connection;
    private Session session;

    private MessageDrivenContext messageDrivenContext;

    @PreDestroy
    protected void preDestroy() throws JMSException {
        session.close();
        connection.close();
    }

    @PostConstruct
    protected void postConstruct() throws JMSException {
        connection = factory.createConnection();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    }

    @Override
    public void setMessageDrivenContext(MessageDrivenContext ctx) throws EJBException {
        this.messageDrivenContext = ctx;
    }

    @Override
    public void ejbRemove() throws EJBException {
    }


    @Override
    public void onMessage(Message message) {
        logger.info("Received message: " + message);
        try {
            if (message.getJMSReplyTo() != null) {
                logger.info("Replying to " + message.getJMSReplyTo());
                final Destination destination = message.getJMSReplyTo();
                final MessageProducer replyProducer = session.createProducer(destination);
                final Message replyMsg;
                if (this.messageDrivenContext != null) {
                    replyMsg = session.createTextMessage(SUCCESS_REPLY);
                } else {
                    replyMsg = session.createTextMessage(FAILURE_REPLY);
                }
                replyMsg.setJMSCorrelationID(message.getJMSMessageID());
                replyProducer.send(replyMsg);
                replyProducer.close();
            }
        } catch (JMSException jmse) {
            throw new RuntimeException(jmse);
        }
    }
}