package org.jboss.as.amq.ejb.mdb.distributedtopic;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.jboss.logging.Logger;

/**
 * User: jpai
 */
@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
		@ActivationConfigProperty(propertyName = "subscriptionDurability", propertyValue = "Durable"),
		@ActivationConfigProperty(propertyName = "clientId", propertyValue = "SubscriberBean"),
		@ActivationConfigProperty(propertyName = "subscriptionName", propertyValue = "SubscriberBean"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = TopicMDB.TOPIC_PHYSICAL_NAME)
})
public class SubscriberBean implements MessageListener {

    private static final Logger logger = Logger.getLogger(TopicMDB.class);

    public static final String TOPIC_PHYSICAL_NAME = "topic1";
    
    public SubscriberBean() {}


    @Override
    public void onMessage(Message message) {
        System.out.println("---> subscriber ");
    }

}