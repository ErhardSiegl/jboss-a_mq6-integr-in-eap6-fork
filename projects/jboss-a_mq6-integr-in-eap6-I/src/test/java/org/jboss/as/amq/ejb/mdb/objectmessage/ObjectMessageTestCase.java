package org.jboss.as.amq.ejb.mdb.objectmessage;

import java.util.Arrays;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Queue;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.as.amq.ejb.mdb.JMSMessagingUtil;
import org.jboss.as.amq.ejb.mdb.cdi.CdiIntegrationMDB;
import org.jboss.as.amq.ejb.mdb.cdi.RequestScopedCDIBean;
import org.jboss.logging.Logger;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.EnterpriseArchive;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Tests that a MDB can get hold of the underlying Object from a {@link ObjectMessage} without any classloading issues.
 * <p/>
 * @author Jaikiran Pai
 * @author Serge Pagop
 */
@RunWith(Arquillian.class)
public class ObjectMessageTestCase {

    private static final Logger logger = Logger.getLogger(ObjectMessageTestCase.class);

    @EJB(mappedName = "java:module/JMSMessagingUtil")
    private JMSMessagingUtil jmsUtil;

    @Resource(mappedName ="java:/queue/MyAMQueue2")
    private Queue objectMessageOfArrayTypeQueue;

    @Resource(mappedName = "java:/queue/MyReplyAMQ2")
    private Queue objectMessageOfArrayTypeReplyQueue;

    @Resource(mappedName = "java:/queue/MyAMQueue")
    private Queue objectMessageQueue;

    @Resource(mappedName = "java:/queue/MyReplyAMQ")
    private Queue objectMessageReplyQueue;


    @Deployment
    public static WebArchive getDeployment() {
        final WebArchive objectmessageWar = ShrinkWrap.create(WebArchive.class, "mdb-objectmessage.war");
        objectmessageWar.addClasses(MDBAcceptingObjectMessageOfArrayType.class, JMSMessagingUtil.class, ObjectMessageTestCase.class, MDBAcceptingObjectMessage.class, SimpleMessageInEarLibJar.class);
        logger.info(objectmessageWar.toString(true));
        logger.info(objectmessageWar.toString(true));
        return objectmessageWar;
    }

    /**
     * Test that the MDB can process a {@link ObjectMessage} which consists of an array of objects,
     * without any classloading issues
     *
     * @throws Exception
     */
    @Test
    public void testObjectMessageWithObjectArray() throws Exception {
        final String goodMorning = "Good morning";
        final String goodEvening = "Good evening";
        // create the message
        final SimpleMessageInEarLibJar[] multipleGreetings = new SimpleMessageInEarLibJar[2];
        final SimpleMessageInEarLibJar messageOne = new SimpleMessageInEarLibJar(goodMorning);
        final SimpleMessageInEarLibJar messageTwo = new SimpleMessageInEarLibJar(goodEvening);
        multipleGreetings[0] = messageOne;
        multipleGreetings[1] = messageTwo;
        // send as ObjectMessage
        this.jmsUtil.sendObjectMessage(multipleGreetings, this.objectMessageOfArrayTypeQueue, this.objectMessageOfArrayTypeReplyQueue);
        // wait for an reply
        final Message reply = this.jmsUtil.receiveMessage(objectMessageOfArrayTypeReplyQueue, 5000);
        // test the reply
        Assert.assertNotNull("Reply message was null on reply queue: " + this.objectMessageOfArrayTypeReplyQueue, reply);
        final SimpleMessageInEarLibJar[] replyMessage = (SimpleMessageInEarLibJar[]) ((ObjectMessage) reply).getObject();
        Assert.assertTrue("Unexpected reply message on reply queue: " + this.objectMessageOfArrayTypeReplyQueue, Arrays.equals(replyMessage, multipleGreetings));

    }

    /**
     * Test that the MDB can process a {@link ObjectMessage} without any classloading issues
     *
     * @throws Exception
     */
    @Test
    public void testObjectMessage() throws Exception {
        final String goodAfternoon = "Good afternoon!";
        // create the message
        final SimpleMessageInEarLibJar message = new SimpleMessageInEarLibJar(goodAfternoon);
        // send as ObjectMessage
        this.jmsUtil.sendObjectMessage(message, this.objectMessageQueue, this.objectMessageReplyQueue);
        // wait for an reply
        final Message reply = this.jmsUtil.receiveMessage(objectMessageReplyQueue, 5000);
        // test the reply
        Assert.assertNotNull("Reply message was null on reply queue: " + this.objectMessageReplyQueue, reply);
        final SimpleMessageInEarLibJar replyMessage = (SimpleMessageInEarLibJar) ((ObjectMessage) reply).getObject();
        Assert.assertEquals("Unexpected reply message on reply queue: " + this.objectMessageReplyQueue, message, replyMessage);
    }
}