package org.jboss.as.amq.ejb.mdb.objectmessage;

import java.io.Serializable;

/**
 * User: jpai
 */
public class SimpleMessageInEarLibJar implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -4960183808619162813L;
	private final String msg;

    public SimpleMessageInEarLibJar(String msg) {
        if (msg == null) {
            throw new IllegalArgumentException("Message cannot be null");
        }
        this.msg = msg;
    }

    public String getMessage() {
        return this.msg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SimpleMessageInEarLibJar that = (SimpleMessageInEarLibJar) o;

        if (!msg.equals(that.msg)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return msg.hashCode();
    }
}