package org.jboss.as.amq.ejb.mdb.distributedtopic;

import java.io.Serializable;

public class Availability implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7796149396755773808L;
	private String name;
	private boolean available;

	public Availability(String name, boolean available) {
		this.name = name;
		this.available = available;
	}

	public boolean isAvailable() {
		return available;
	}

	public String getName() {
		return name;
	}

}
