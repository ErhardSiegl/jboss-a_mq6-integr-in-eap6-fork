package org.jboss.as.amq.ejb.mdb.cdi;

import javax.enterprise.context.RequestScoped;

/**
 * @author Stuart Douglas
 */
@RequestScoped
public class RequestScopedCDIBean {

    public String sayHello() {
        return CdiIntegrationMDB.REPLY;
    }

}
