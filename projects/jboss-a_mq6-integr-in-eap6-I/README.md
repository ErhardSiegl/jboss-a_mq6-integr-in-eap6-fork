jboss-a_mq6-intgr-in-eap6 Part I: Integrate JBoss A-MQ RAR in JBoss EAP 6.1.0.BETA
==================================================================================
Author: Serge Pagop, Torben Jäger
Level: Advanced
Technologies: JMS, EJB, MDB
Summary: Demonstrates the integration of ActiveMQ in JBoss EAP 6.1.0.BETA
Target Product: JBoss A-MQ 6.0.0 and JBoss EAP 6.1.0.Beta

Pre-Reqs
========
* Maven 3.0.3 for your project
* JBoss EAP 6.1.0.BETA
* JBoss A-MQ 6.0.0/ActiveMQ Resource Adapter installed
* Database is configured to work with H2 Database, but you can change that in "support/broker-config.xml" file for your favorite Database

What is it?
-----------

This example demonstrates the integration of JBoss A-MQ/ActiveMQ RAR in a standalone EAP 6 server configuration

Why users need JBoss A-MQ integration in JBoss EAP 6
----------------------------------------------------

1) A-MQ was already a standard messaging platform in their IT
2) Requirement for an enterprise database for message store
   - Need for long term persistence with extreme reliability
   - Need to use SQL Query to find information about MSGs
   - Expertise in the administration of relational database exists
3) Requirement for creating a redundant master/slave topology of brokers by using a shared databases



Test your integration with the existed Arquillian tests
-------------------------------------------------------

1) Import the project "jboss-a_mq6-intgr-in-eap6-I" from "projects" with JBoss Developer Studio

2) Open the "arquillian.xml" file from "jboss-a_mq6-intgr-in-eap6-I/src/test/resources/" folder and change the "<property name="jbossHome">" to your the $JBOSS_HOME
    - <property name="jbossHome">/path/to/jboss-a_mq6-integr-in-eap6/target/jboss-eap-6.1</property>

3) And you can run the arquillian tests with `mvn test -P testAll`



How you can start JBoss EAP 6 with embedded JBoss A-MQ/ActiveMQ with our customized Profile?
--------------------------------------------------------------------------------------------

1. Open a command line and navigate to the root of the JBoss server directory target/jboss-eap-6.0.
2. The following shows the command line to start the server with the profile:

        For Linux:   JBOSS_HOME/bin/standalone.sh -c standalone-amq580.xml
        For Windows: JBOSS_HOME\bin\standalone.bat -c standalone-amq580.xml
        
Look at the JBoss Application Server console or Server log and you should see log messages like the following:        

13:10:37,074 INFO  [org.jboss.as.connector.deployers.RaXmlDeployer] (MSC service thread 1-4) IJ020001: Required license terms for file:/Volumes/Macintosh%20HD/Users/spagop/Projects/redhat/jboss-a_mq6-integr-in-eap6/target/jboss-eap-6.1/standalone/tmp/vfs/temp62b5bb2f9b97ff85/amq-5.8.0.redhat-60024.rar-f0d7e327b5daae2/contents/
13:10:37,087 INFO  [org.jboss.as.connector.deployment] (MSC service thread 1-4) JBAS010406: Registered connection factory java:jboss/exported/ConnectionFactory
13:10:37,089 WARN  [org.jboss.as.connector.deployers.RaXmlDeployer] (MSC service thread 1-4) IJ020016: Missing <recovery> element. XA recovery disabled for: java:jboss/exported/ConnectionFactory
13:10:37,092 INFO  [org.jboss.as.connector.deployment] (MSC service thread 1-4) JBAS010405: Registered admin object at java:/queue/MyReplyAMQ2
13:10:37,094 INFO  [org.jboss.as.connector.deployment] (MSC service thread 1-4) JBAS010405: Registered admin object at java:/queue/MyAMQueue2
13:10:37,094 INFO  [org.jboss.as.connector.deployment] (MSC service thread 1-4) JBAS010405: Registered admin object at java:/queue/MyAMQueue
13:10:37,095 INFO  [org.jboss.as.connector.deployment] (MSC service thread 1-4) JBAS010405: Registered admin object at java:/queue/MyReplyAMQ
13:10:37,096 INFO  [org.jboss.as.connector.deployment] (MSC service thread 1-4) JBAS010405: Registered admin object at java:jboss/eis/ao/ActiveMQTopic
13:10:37,098 INFO  [org.jboss.as.connector.deployers.RaXmlDeployer] (MSC service thread 1-4) IJ020002: Deployed: file:/Volumes/Macintosh%20HD/Users/spagop/Projects/redhat/jboss-a_mq6-integr-in-eap6/target/jboss-eap-6.1/standalone/tmp/vfs/temp62b5bb2f9b97ff85/amq-5.8.0.redhat-60024.rar-f0d7e327b5daae2/contents/
13:10:37,100 INFO  [org.jboss.as.connector.deployment] (MSC service thread 1-7) JBAS010401: Bound JCA AdminObject [java:/queue/MyAMQueue2]
13:10:37,101 INFO  [org.jboss.as.connector.deployment] (MSC service thread 1-3) JBAS010401: Bound JCA ConnectionFactory [java:jboss/exported/ConnectionFactory]
13:10:37,101 INFO  [org.jboss.as.connector.deployment] (MSC service thread 1-3) JBAS010401: Bound JCA AdminObject [java:/queue/MyAMQueue]
13:10:37,101 INFO  [org.jboss.as.connector.deployment] (MSC service thread 1-3) JBAS010401: Bound JCA AdminObject [java:jboss/eis/ao/ActiveMQTopic]
13:10:37,100 INFO  [org.jboss.as.connector.deployment] (MSC service thread 1-4) JBAS010401: Bound JCA AdminObject [java:/queue/MyReplyAMQ]
13:10:37,101 INFO  [org.jboss.as.connector.deployment] (MSC service thread 1-7) JBAS010401: Bound JCA AdminObject [java:/queue/MyReplyAMQ2]
13:10:37,131 INFO  [org.jboss.as.server] (ServerService Thread Pool -- 26) JBAS018559: Deployed "amq-5.8.0.redhat-60024.rar" (runtime-name : "amq-5.8.0.redhat-60024.rar")
13:10:37,137 INFO  [org.jboss.as] (Controller Boot Thread) JBAS015961: Http management interface listening on http://127.0.0.1:9990/management
13:10:37,137 INFO  [org.jboss.as] (Controller Boot Thread) JBAS015951: Admin console listening on http://127.0.0.1:9990
13:10:37,137 INFO  [org.jboss.as] (Controller Boot Thread) JBAS015874: JBoss EAP 6.1.0.Beta1 (AS 7.2.0.Final-redhat-4) started in 2723ms - Started 185 of 241 services (55 services are passive or on-demand)
