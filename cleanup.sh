#!/bin/sh

cd `dirname $0` || exit 1

rm support/modules/system/layers/base/org/apache/activemq/main/*.jar
rm -r target
